import request from '@/config/axios'
import { useCache } from '@/hooks/web/useCache'
const { wsCache } = useCache()
//首页最新资讯
export const newsfeed = (params: any): Promise<IResponse> => {
  return request.get({ url: '/newsfeed', params, module: 'jy-api',headersType:"multipart/form-data",menuid:wsCache.get("menuid")  })
}
//首页最新资讯详情
export const newsfeedsee = (params: any): Promise<IResponse> => {
  return request.get({ url: '/newsfeedsee', params, module: 'jy-api',menuid:wsCache.get("menuid") })
}
//获取首页
export const homeConfig = (): Promise<IResponse> => {
  return request.get({ url:'/home', module: 'jy-api',menuid:wsCache.get("menuid")})
}
//首页通知公告
export const notices = (): Promise<IResponse> => {
  return request.get({ url: '/notices', module: 'jy-api',menuid:wsCache.get("menuid") })
}
//首页通知公告详情
export const noticessess = (params: any): Promise<IResponse> => {
  return request.get({ url: '/notices/see', params, module: 'jy-api',menuid:wsCache.get("menuid") })
}
//常用功能配置规则匹配
export const commonconfig = (): Promise<IResponse> => {
  return request.get({ url: '/commonconfig',  module: 'jy-api', })
}
//菜单配置
export const commonconfigDel = (params: any): Promise<IResponse> => {
  return request.post({ url: '/commonconfig/deal', data:params, module: 'jy-api',headersType:"multipart/form-data",menuid:wsCache.get("menuid") })
}
