import request from '@/config/axios'
import { useCache } from '@/hooks/web/useCache'
const { wsCache } = useCache()
//参数类型
import { businListParam } from './applicationModel'
//数据审批列表 http://192.168.0.88:9588
export const getTableListApi = (params:businListParam): Promise<IResponse> => {
  return request.get({ url: '/approval', params, module: 'jy-api',menuid:wsCache.get("menuid") })
}

//审批详情
export const approvalsee = (params:any): Promise<IResponse> => {
  return request.get({ url: '/approvalsee', params, module: 'jy-api',menuid:wsCache.get("menuid") })
}


//审批通过和驳回http://192.168.0.88:9588

export const approvalstate = (params:any): Promise<IResponse> => {
  return request.post({ url: '/approval/state', data:params, module: 'jy-api',headersType: "multipart/form-data",menuid:wsCache.get("menuid") })
}