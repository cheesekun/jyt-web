import request from '@/config/axios'
import { businListParam } from './businessModel'
import { useCache } from '@/hooks/web/useCache'
const { wsCache } = useCache()
// 产品核算列表
enum Api {
  getProductlist = '/productcalculate'
}
export const getTableListApi = (params: any): Promise<IResponse> => {
  return request.get({ url: '/productcalculate', params, module: 'jy-api',menuid:wsCache.get("menuid")  })
}
//核算删除
export const delprocalculate = (params: any): Promise<IResponse> => {
  return request.post({ url: '/productcalculate/del', data: params, module: 'jy-api', headersType: "multipart/form-data",menuid:wsCache.get("menuid")  })
}

//新增
export const addcalculate = (params: any): Promise<IResponse> => {
  return request.post({ url: '/productcalculate/add', data: params, module: 'jy-api', headersType: "multipart/form-data",menuid:wsCache.get("menuid")  })
}
//核算编辑
export const editcalculate = (params: any): Promise<IResponse> => {
  return request.post({ url: '/productcalculate/edit', data: params, module: 'jy-api', headersType: "multipart/form-data",menuid:wsCache.get("menuid")  })
}
//查看
export const detailscalculate = (params: any): Promise<IResponse> => {
  return request.get({ url: '/productcalculatesee', params, module: 'jy-api',menuid:wsCache.get("menuid") })
}

//核算删除http://192.168.0.88:9588
export const productdatadel = (params: any): Promise<IResponse> => {
  return request.post({ url: '/productdata/del', data:params, module: 'jy-api',headersType: "multipart/form-data",menuid:wsCache.get("menuid") })
}
//撤回申请  提交审核
export const productdatappli = (params: any): Promise<IResponse> => {
  return request.post({ url: '/productcalculate/state', data:params, module: 'jy-api',headersType: "multipart/form-data",menuid:wsCache.get("menuid") })
}
/**
 * 数据管理
 */
//列表
export const productdataApi = (params: any): Promise<IResponse> => {
  return request.get({ url: '/productdata', params, module: 'jy-api',menuid:wsCache.get("menuid") })
}
//数据管理添加http://192.168.0.88:9588/
export const productdataadd = (params: any): Promise<IResponse> => {
  return request.post({ url: '/productdata/add', data:params, module: 'jy-api',headersType: "multipart/form-data",menuid:wsCache.get("menuid") })
}
// 数据管理编辑
export const productdataedit = (params: any): Promise<IResponse> => {
  return request.post({ url: '/productdata/edit', data:params, module: 'jy-api',headersType: "multipart/form-data"})
}