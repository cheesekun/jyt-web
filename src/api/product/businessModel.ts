//查询列表参数类型
export interface businListParam {
    filterProductName:string,
    filterProductNo:string,
    filterState:string | number,
    pageIndex:string | number,
    pageSize:string | number,
    value1:string | number,
    filterWeekStart:string | number,
    filterWeekEnd:string | number
}

//返回
export interface businList {
    list:any
}

//add每行数据格式
export interface businessModel {
    id: string | number,
    number:string | number,
    unit:string | number,
    week_start:string | number,
    week_end:string | number,
    state:string | number,
    modify_time:string | number,
    product_name:string,
    product_no:string,
    product_spec:string,
}