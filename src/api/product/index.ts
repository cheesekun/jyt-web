import request from '@/config/axios'
import type { TableData } from './types'
import { useCache } from '@/hooks/web/useCache'
const { wsCache } = useCache()
//产品管理列表
export const getTableListApi = (params: any): Promise<IResponse> => {
  return request.get({ url: '/product', params, module: 'jy-api',menuid:wsCache.get("menuid")  })
}
//产品详情
export const getProducts = (params: any): Promise<IResponse> => {
  return request.get({ url: '/productsee', params, module: 'jy-api',menuid:wsCache.get("menuid")  })
}
//原材料列表

export const getproductmaterial = (params: any): Promise<IResponse> => {
  return request.get({ url: '/productmaterial', params, module: 'jy-api',menuid:wsCache.get("menuid")  })
}
//原材料删除
export const delproductmaterial = (params: any): Promise<IResponse> => {
  return request.post({ url: '/productmaterial/del', data:params, module: 'jy-api',headersType:"multipart/form-data",menuid:wsCache.get("menuid")  })
}
//原材料编辑
export const editproductmaterial = (params: any): Promise<IResponse> => {
  return request.post({ url: '/productmaterial/edit', data:params, module: 'jy-api',headersType:"multipart/form-data",menuid:wsCache.get("menuid")  })
}
//原材料新增
export const editproductmaterialadd = (params: any): Promise<IResponse> => {
  return request.post({ url: '/productmaterial/add', data:params, module: 'jy-api',headersType:"multipart/form-data",menuid:wsCache.get("menuid")  })
}
  
//文件上传
export const  uploadFileApi = (params: any): Promise<IResponse> => {
  return request.post({ url: '/file/upload', data:params ,headersType:"multipart/form-data",menuid:wsCache.get("menuid") })
}

