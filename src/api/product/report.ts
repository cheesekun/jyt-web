import request from '@/config/axios'
import { useCache } from '@/hooks/web/useCache'
const { wsCache } = useCache()
//参数类型
import { businListParam,reportDetails,reportDel} from './reportModel'
// 产品报告列表
export const getTableListApi = (params:businListParam): Promise<IResponse> => {
  return request.get({ url: '/calculatereport', params, module: 'jy-api' })
}

//核算报告详情
export const getreportsee = (params:reportDetails): Promise<IResponse> => {
  return request.get({ url: '/calculatereportsee', params, module: 'jy-api' })
}
//核算报告删除
export const delcalculatereport = (params:reportDel): Promise<IResponse> => {
  return request.post({ url: '/calculatereport/del', data:params, module: 'jy-api',headersType: "multipart/form-data" })
}  
//核算周期列表
export const getCalculatereportweeks = (params:any): Promise<IResponse> => {
  return request.get({ url: '/calculatereportweeks', params, module: 'jy-api' })
}
//添加http://192.168.0.88:9588
export const calculatereportAdd = (params:any): Promise<IResponse> => {
   return request.post({ url:'/calculatereport/add', data:params, module: 'jy-api',headersType: "multipart/form-data" })
}
//编辑
export const calculatereportEdit = (params:any): Promise<IResponse> => {
  return request.post({ url:'/calculatereport/edit', data:params, module: 'jy-api',headersType: "multipart/form-data" })
}
//生成报告
export const productcalculatecreate = (params:any): Promise<IResponse> => {
  return request.get({ url:'/productcalculatecreate', params, module: 'jy-api' })
}