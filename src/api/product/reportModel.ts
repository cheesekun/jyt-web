//查询列表参数类型
export interface businListParam {
    filterReportName:string,  //报告名称
    filterProductName:string,  //核算产品名称
    filterType:string | number,  //搜索类型
    pageIndex:number,
    pageSize:number,
}

//返回
export interface businList {
    list:any
}

//核算报告详情  传递的参数
export interface reportDetails {
    id:any
}

//核算报告删除
export interface reportDel {
    id:any
}
//定义后端的返回格式
export interface reportDetail {
    list:[]
}

//每行数据格式
export interface businessModel {
    id: string | number,
    number:string | number,
    unit:string | number,
    week_start:string | number,
    week_end:string | number,
    state:string | number,
    modify_time:string | number,
    product_name:string,
    product_no:string,
    product_spec:string,
}