import request from '@/config/axios'
import { useCache } from '@/hooks/web/useCache'
const { wsCache } = useCache()
// 获取所有设置权限的列表
export const getAuthApi = (): Promise<IResponse> => {
  return request.get({ url: '/auth/list',menuid:wsCache.get("menuid") })
}




