import request from '@/config/axios'
import { AddRoleType, EditRoleType, DeleteRoleType, RoleResponse} from './types' 
import { useCache } from '@/hooks/web/useCache'
const { wsCache } = useCache()
// 新建角色并绑定权限
export const addRole = (data: AddRoleType): Promise<IResponse> => {
    return request.post({ url: '/role/add', data,menuid:wsCache.get("menuid") })
  }

// 编辑角色并绑定权限
export const editRole = (data: EditRoleType): Promise<IResponse> => {
    return request.post({ url: '/role/edit', data,menuid:wsCache.get("menuid") })
  }

// 删除角色通过ID
export const deleteRole = (data: DeleteRoleType): Promise<RoleResponse> => {
    return request.post({ url: '/role/delete', data,menuid:wsCache.get("menuid") })
  }

// 请求角色列表
export const getRoleList = (data: any): Promise<IResponse> => {
    return request.get({ url: '/roles/getList', data,menuid:wsCache.get("menuid") })
  }

  
