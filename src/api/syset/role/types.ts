export type AddRoleType = {
    title:string
    bindStr:string
} 

export type EditRoleType = {
    title:string
    bindStr:string
    editId:number
} 

export type DeleteRoleType = {
    deleteId:number
}

export interface RoleResponse<T = any> {
    code: string
    data: T extends any ? T : T & any
    message: string
  }
