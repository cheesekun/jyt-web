import request from '@/config/axios'
import { getUserType } from './intefac'
import { businListParam } from './businessModel'
import { useCache } from '@/hooks/web/useCache'
const { wsCache } = useCache()

//获取用户列表

export const getUserLists = (params: getUserType): Promise<IResponse> => {
     return request.get({ url: '/user', params,module: 'jy-api',menuid:wsCache.get("menuid") })
  }
  //更新用户状态
export const userState = (params:any): Promise<IResponse> => {
   return request.post({ url: '/user/state', data:params,module: 'jy-api',headersType:"multipart/form-data",menuid:wsCache.get("menuid") })
}
//新增用户
export const userAdd = (params:any): Promise<IResponse> => {
   return request.post({ url: '/user/add', data:params,module: 'jy-api',headersType:"multipart/form-data",menuid:wsCache.get("menuid") })
}