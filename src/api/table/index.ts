import request from '@/config/axios'
import type { TableData } from './types'
import { useCache } from '@/hooks/web/useCache'
const { wsCache } = useCache()
export const getTableListApi = (params: any): Promise<IResponse> => {
  return request.get({ url: '/example/list', params,menuid:wsCache.get("menuid") })
}

//新增产品保存
export const saveTableApi = (data: Partial<TableData>): Promise<IResponse> => {
  return request.post({ url: '/product/add', data ,headersType:"multipart/form-data",module:"jy-api",menuid:wsCache.get("menuid")})
}

//产品管理编辑保存
export const saveEditApi = (data: Partial<TableData>): Promise<IResponse> => {
  return request.post({ url: '/product/edit', data ,headersType:"multipart/form-data",module:"jy-api",menuid:wsCache.get("menuid")})
}
export const getTableDetApi = (id: string): Promise<IResponse<TableData>> => {
  return request.get({ url: '/example/detail', params: { id },menuid:wsCache.get("menuid") })
}

//表格删除（多个）
export const delTableListApi = (ids: string[] | number[]): Promise<IResponse> => {
  return request.post({ url: '/example/delete', data: { ids },menuid:wsCache.get("menuid") })
}

//产品状态更改
export const updataState = (params: any): Promise<IResponse> => {
  return request.post({ url: '/product/state', data:params,headersType:"multipart/form-data",module:"jy-api",menuid:wsCache.get("menuid")})
}