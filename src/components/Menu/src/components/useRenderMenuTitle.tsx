import type { RouteMeta } from 'vue-router'
import { Icon } from '@/components/Icon'
import { useI18n } from '@/hooks/web/useI18n'
import { useRouter } from 'vue-router'
import { useCache } from '@/hooks/web/useCache' 
const { wsCache } = useCache()
export const useRenderMenuTitle = () => {
  const renderMenuTitle = (meta: RouteMeta) => {
    const { t } = useI18n()
     const { title = 'Please set title', icon } = meta
     const { push, currentRoute } = useRouter()
     //将点击的菜单menuid存储到请求头中
     wsCache.set("menuid",currentRoute.value.meta.id)
     //首先现在是没有获取菜单的id
    // console.log("获取当前左侧点击的左侧菜单",currentRoute.value.meta.id);
    // console.log("获取当前点击的菜单id",currentRoute.value.meta.id);
    return icon ? (
      <>
        <Icon icon={meta.icon}></Icon>
        <span class="v-menu__title">{t(title as string)}</span>
      </>
    ) : (
      <span class="v-menu__title">{t(title as string)}</span>
    )
  }

  return {
    renderMenuTitle
  }
}
