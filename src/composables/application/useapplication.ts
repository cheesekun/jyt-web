import { ref } from 'vue'
//定义格式
import { businessModel } from "@/api/product/applicationModel"
import useInstance from '@/hooks/useInstance';
/**
 * 
 * businListParam 定义查询的参数类型
 * businList：返回类型的定义
 */
import {  businListParam,businList } from "@/api/product/applicationModel"
export default function useDept(getDeptList,searchParm:businListParam) {
    const { global, proxy } = useInstance();
    const addDeptRef = ref<{ show: (type: string, row?: businessModel) => void }>();
    //搜索
    const serachBtn = () => {
        getDeptList();
    }
    //重置
    const resetBtn = () =>{
        //清空搜索框
        searchParm.filterProductName= '',
        searchParm.filterProductNo= '',
        searchParm.filterWeekStart= '',
        searchParm.filterWeekEnd= '',
        searchParm.value1 = ''
        getDeptList();
    }

    return {
        serachBtn,
        resetBtn
    }
}