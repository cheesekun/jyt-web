/**
 * 表格列表的业务逻辑
 */
import { reactive, onMounted, ref, nextTick } from 'vue'
import {  businListParam,businList } from "@/api/product/applicationModel"
import { getTableListApi } from '@/api/product/application'
export default function useDepaTable() {
    //表格的高度
    const tableHeigth = ref(0);
    const loading = ref(false)
    const Total = ref(0)
    //定义列表查询参数
    const searchParm = reactive<businListParam>({
       filterProductName:"",
       filterProductNo:"",
       filterWeekStart:"",
       filterWeekEnd:"",
       value1:"",
       pageIndex:1,
       pageSize:10
    })
    //定义表格的数据
    const tableData = reactive<businList>({
        list: []
    })
    //获取表格数据
    const getDeptList = async () => {
        loading.value = true;
        await getTableListApi(searchParm).then(res =>{
            if (res && res.code == '200') {
               tableData.list = res.data.list;
               Total.value =  res.data.total;
               loading.value = false;
        }
        });
    }
    const handleCurrentChange = (val =>{
       searchParm.pageIndex = val;
       getDeptList()
    })
    const handleSizeChange = (val =>{
       searchParm.pageSize = val;
       getDeptList()
    })
    //首次加载
    onMounted(() => {
        getDeptList();
    })
    return {
        searchParm,
        tableData,
        getDeptList,
        tableHeigth,
        handleCurrentChange,
        handleSizeChange,
        Total,
        loading
    }
}