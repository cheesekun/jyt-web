import { ref } from 'vue'
//定义格式
import { businessModel } from "@/api/product/reportModel"
import useInstance from '@/hooks/useInstance';
/**
 * 
 * businListParam 定义查询的参数类型
 * businList：返回类型的定义
 */
import {  businListParam,businList } from "@/api/product/reportModel"
import { delReportscol } from "./useReportModel"
import {  delcalculatereport } from '@/api/product/report'
export default function useDept(getDeptList,searchParm:businListParam) {
    const { global, proxy } = useInstance();
    const addDeptRef = ref<{ show: (type: string, row?: businessModel) => void }>();
    //搜索
    const serachBtn = () => {
        getDeptList();
    }
    //重置
    const resetBtn = () =>{
        //清空搜索框
        searchParm.filterReportName= '',
        searchParm.filterProductName= '',
        searchParm.filterType= '',
        getDeptList();
    }
    //刪除
    const delReports = async(id:delReportscol) =>{
        await delcalculatereport(id).then(res => {
           console.log("删除状态",res);
           
        })

    }

    return {
        delReports,
        serachBtn,
        resetBtn
    }
}