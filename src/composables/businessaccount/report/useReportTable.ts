/**
 * 表格列表的业务逻辑
 */
import { reactive, onMounted, ref, nextTick } from 'vue'
import { businListParam, businList, reportDetails, reportDetail } from "@/api/product/reportModel"
import { getTableListApi, getreportsee } from '@/api/product/report'
export default function useDepaTable() {
    //表格的高度
    const tableHeigth = ref(0);
    const loading = ref(false)
    const Total = ref(0)
    const report_types = reactive({
        data: []
    })
    const products = reactive({
        datas:[]
    })
    //定义列表查询参数
    const searchParm = reactive<businListParam>({
        filterReportName: "",  //报告名称
        filterProductName: "",  //核算产品名称
        filterType: "",  //搜索类型
        pageIndex: 1,
        pageSize: 10
    })
    //定义表格的数据
    const tableData = reactive<businList>({
        list: []
    })
    //定义详情的集合
    const getReportdata = reactive<reportDetail>({
        list: []
    })
    //获取表格数据
    const getDeptList = async () => {
        loading.value = true;
        await getTableListApi(searchParm).then(res => {
            if (res && res.code == '200') {
                loading.value = false;
                tableData.list = res.data.list;
                products.datas = res.data.products;
                report_types.data = res.data.report_types;
                Total.value = res.data.total;
            }
        });
    }
    //获取查看详情的数据   async (id:reportDetails)  声明传递的参数 await将声明的参数携带上去
    const getReportdeta = async (id: reportDetails) => {
        await getreportsee(id).then(res => {
            getReportdata.list = res.data.list;
            // console.log("获取详情数据",res);
        })
    }

    
    const handleCurrentChange = (val => {
        searchParm.pageIndex = val;
        getDeptList()
    })
    const handleSizeChange = (val => {
        searchParm.pageSize = val;
        getDeptList()
    })
    //首次加载
    onMounted(() => {
        getDeptList();
    })
    return {
        getReportdeta,
        getReportdata,
        searchParm,
        products,
        tableData,
        report_types,
        getDeptList,
        tableHeigth,
        handleCurrentChange,
        handleSizeChange,
        Total,
        loading
    }
}