/**
 * 针对操作
 */
import { businessModel } from '@/api/product/businessModel'
import { reactive } from 'vue'
export default function useBaseModel() {
    //表单验证
    const rules = reactive({
        product_name: [{
            required: true,
            message: '请选择核算产品',
            trigger: 'change',
        }],
        number: [{
            required: true,
            message: '请输入核算数量',
            trigger: 'change',
        }]
    })

    //新增和编辑表单数据
    const dialogModel = reactive<businessModel>({
        id:"",
        number:"",
        unit:"",
        week_start:"",
        week_end:"",
        state:"",
        modify_time:"",
        product_name:"",
        product_no:"",
        product_spec:"",
    })


    return {
        rules,
        dialogModel
    }
}