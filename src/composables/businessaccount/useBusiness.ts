import { ref } from 'vue'
import { businessModel } from "@/api/product/businessModel"
import useInstance from '@/hooks/useInstance';
import {  businListParam,businList } from "@/api/product/businessModel"
export default function useDept(getDeptList,searchParm:businListParam) {
    const { global, proxy } = useInstance();
    const addDeptRef = ref<{ show: (type: string, row?: businessModel) => void }>();
    //搜索
    const serachBtn = () => {
        getDeptList();
    }
    //重置
    const resetBtn = () =>{
        //清空搜索框
        searchParm.filterProductName= '',
        searchParm.filterProductNo= '',
        searchParm.filterState= '',
        searchParm.filterWeekStart= '',
        searchParm.filterWeekEnd= '',
        searchParm.value1= '',
        getDeptList();
    }

    return {
        serachBtn,
        resetBtn
    }
}