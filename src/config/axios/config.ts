const config: {
  base_url: {
    base: string
    dev: string
    pro: string
    test: string
  }
  result_code: number | string
  error_code: number | string
  default_headers: AxiosHeaders
  request_timeout: number,
  font_path_err:number | string,
  serve_code :number | string,
  requirenum:number | string,
  errljcatch:number | string,
  opererr:number | string,
} = {
  /**
   * api请求基础路径
   */
  base_url: {
    // 开发环境接口前缀
    base: '/api',

    // 打包开发环境接口前缀
    dev: '',

    // 打包生产环境接口前缀
    pro: '',

    // 打包测试环境接口前缀
    test: ''
  },

  /**
   * 接口成功返回状态码
   */
  result_code: '200',

  /**
   * 接口成功返回状态码
   */
  error_code: '403',
  /**
   * 接口成功返回的状态码
   */
   serve_code:'500',

   /**
    * 接口返回成功返回的状态码
    */
   font_path_err :'404',
   /**
    * 表单必填
    */
   requirenum:422,
   /**
    * 成功但业务逻辑异常
    * 
    */
   errljcatch:2001,
   /**
    * 操作失败
    */
   opererr:201,
  /**
   * 接口请求超时时间
   */
  request_timeout: 60000,

  /**
   * 默认接口请求类型
   * 可选值：application/x-www-form-urlencoded multipart/form-data
   */
  default_headers: 'application/json'
}

export { config }
