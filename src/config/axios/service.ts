import axios, {
  AxiosInstance,
  AxiosRequestConfig,
  AxiosRequestHeaders,
  AxiosResponse,
  AxiosError
} from 'axios'

import qs from 'qs'

import { config } from './config'

import { ElMessage } from 'element-plus'

import { useCache } from '@/hooks/web/useCache'
const { wsCache } = useCache()
import { useAuthStore } from '@/store/modules/auth'

import { useTagsViewStore } from '@/store/modules/tagsView'

import { resetRouter } from '@/router'



const tagsViewStore = useTagsViewStore()

const authStore = useAuthStore()

const { result_code, error_code,base_url, serve_code,font_path_err,requirenum,opererr,errljcatch } = config

export const PATH_URL = base_url[import.meta.env.VITE_API_BASEPATH]

// 创建axios实例
const service: AxiosInstance = axios.create({
  baseURL: PATH_URL, // api 的 base_url
  timeout: config.request_timeout // 请求超时时间
})
// request拦截器
service.interceptors.request.use(
  (config: AxiosRequestConfig) => {
    if (
      config.method === 'post' &&
      (config.headers as AxiosRequestHeaders)['Content-Type'] ===
      'application/x-www-form-urlencoded'
    ) 
    {
      config.data = qs.stringify(config.data)
    }

    if (config.url != '/user/login') {
      ;(config.headers as AxiosRequestHeaders)['Token'] = wsCache.get(authStore.getToken);
      ;(config.menuid) = wsCache.get("menuid")
    }

    // get参数编码
    if (config.method === 'get' && config.params) {
      let url = config.url as string
      url += '?'
      const keys = Object.keys(config.params)
      for (const key of keys) {
        if (config.params[key] !== void 0 && config.params[key] !== null) {
          url += `${key}=${encodeURIComponent(config.params[key])}&`
        }
      }
      url = url.substring(0, url.length - 1)
      config.params = {}
      config.url = url
    }
    return config
  },
  (error: AxiosError) => {
    // Do something with request error
    console.log(error) // for debug
    Promise.reject(error)
  }
)

// response 拦截器
service.interceptors.response.use(
  (response: AxiosResponse<any>) => {
    if (response.config.responseType === 'blob') {
      // 如果是文件流，直接过
      return response
      //状态等于200
    } else if (response.data.code === result_code) {
      return response.data
       //状态等于500
    }else if(response.data.code ==  serve_code){
        ElMessage.error(response.data.message)
        //状态码404
    }else if(response.data.code == font_path_err){
      ElMessage.error(response.data.message)
      //422表填必填
    }else if(response.data.code == requirenum){
      ElMessage.error(response.data.message)
      //201操作异常
    }else if(response.data.code == opererr){
      ElMessage.error(response.data.message)
      //2001成功但业务逻辑异常
    }else if(response.data.code == opererr){
      ElMessage.error(response.data.message)
    }
    //Token过期
    else if(response.data.code == error_code){
         ElMessage.error(response.data.message)
         wsCache.clear()
         tagsViewStore.delAllViews()
         resetRouter() // 重置静态路由表
         location.reload()
    }
    //正常執行----
    ElMessage.success(response.data.message)
    //获取任何请求的response后面的字段
     return response.data
  },
  (error: AxiosError) => {
    //异常处理
    console.log('err' + error) // for debug
    ElMessage.error(error.message)
    //异常处理如果获取不到response响应后面的值 要抛出去 return response.data.xxx
    return Promise.reject(error)
  }
)
export { service }
