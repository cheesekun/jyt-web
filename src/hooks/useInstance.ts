import { getCurrentInstance,ComponentInternalInstance  } from "vue-demi";

export default function useInstance(){
    const {appContext,proxy} =  getCurrentInstance() as ComponentInternalInstance 
    const global = appContext.config.globalProperties;
    return{
        proxy,
        global
    }
}