import type { App } from 'vue'

// 需要全局引入一些组件，如ElScrollbar，不然一些下拉项样式有问题
import { ElLoading, ElScrollbar } from 'element-plus'
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css'
const plugins = [ElLoading]

const components = [ElScrollbar]

export const setupElementPlus = (app: App<Element>) => {
  plugins.forEach((plugin) => {
    app.use(plugin)
    app.use(ElementPlus)
  })

  components.forEach((component) => {
    app.component(component.name, component)
  })
}
