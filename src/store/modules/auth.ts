import { defineStore } from 'pinia'
import { store } from '../index'

export interface AuthState {
  token: string
}

export const useAuthStore = defineStore('auth', {
  state: (): AuthState => {
    return {
      token: 'token'
    }
  },
  getters: {
    getToken(): string {
      return this.token
    },
  },
  actions: {
    setToken(token: string) {
      this.token = token
    },
}
})

export const useDictStoreWithOut = () => {
  return useAuthStore(store)
}
