export type contextMenuSchema = {
  product_name: any
  list:[]
  length:string
  [x: string]: any 
  disabled?: boolean
  divided?: boolean
  icon?: string
  label: string
  command?: (item: contextMenuSchema) => void
}
