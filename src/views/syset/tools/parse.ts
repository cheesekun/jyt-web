
const parseRolesToDisable = (objArr:any, disBool:boolean)=>{

    const arr1:any = []
    for(let i=0;i<objArr.length;i++){
     
        const obj1 = parseRoleToDisable(objArr[i],disBool)
        arr1.push(obj1)
    }

    
    return arr1
}


// 对树进行禁用操作
const parseRoleToDisable = (obj:{}, disBool:boolean)=>{

    if(Reflect.has(obj, 'children')){
       obj['children'] = parseRolesToDisable(obj['children'], disBool)
    }
    obj['disabled'] = disBool

    return obj

}




export{
    parseRolesToDisable
}