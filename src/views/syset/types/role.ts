
enum Switch {
    Edit,
    List,
    Reload
}

enum BtnType {
    Check,
    Edit,
    Delete,
    Add,
    Cancel
}

export {
    Switch,
    BtnType
}